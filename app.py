from decouple import config
from requests import Session
import plotly.graph_objects as go
import logging
# debugging only
import pprint
import sys


logger = logging.basicConfig(level=logging.DEBUG)
pp = pprint.PrettyPrinter(indent=4)

class TenableIOSession():
    def __init__(self):
        self._SECRET_KEY = config('SECRET_KEY')
        self._ACCESS_KEY = config('ACCESS_KEY')
        self.session = Session()
        self.session.headers = {
            'accept': 'application/json',
            'x-apikeys': 'accessKey={ACCESS};secretKey={SECRET}'.format(ACCESS=self._ACCESS_KEY, SECRET=self._SECRET_KEY)
        }
        self.base_url = "https://cloud.tenable.com"


class Lumin(TenableIOSession):
    # Solution methods
    def get_solutions(self, limit) -> dict:
        return self.session.get(self.base_url + '/lumin/solutions', params=f'limit={limit}').json()

    # Cyber Exposure Score methods
    def get_ces_current(self):
        pass

    def get_ces_trend(self):
        pass

    def get_ces_detail(self):
        pass

    def get_ces_by_asset_group(self, tag_id=None):
        result = self.session.get(self.base_url + '/lumin/widgets/exposure-score-breakdown', params=f'tagId={tag_id}').json()
        pp.pprint(result)

    def get_available_tags(self):
        result = self.session.get(self.base_url + '/lumin/assets/tags').json()
        return result['tags']

    # Lumin Scan Endpoints
    def get_scan_distribution(self) -> dict:
        return self.session.get(self.base_url + '/lumin/distribution').json()


class LuminExports(Lumin):
    def __init__(self):
        self.lumin = Lumin()

    def generate_solutions_graph(self, num):
        if num > 51:
            sys.exit('Too many results. Please choose a number below 51.')

        solutions = self.lumin.get_solutions(num)['solutions']
        titles = list()
        descriptions = list()
        asset_counts = list()
        vuln_counts = list()
        exploit_code_maturity = list()
        vpr_scores = list()
        cvss_max_scores = list()

        for solution in solutions:
            titles.append(solution['title'])
            descriptions.append(solution['description'])
            asset_counts.append(solution['asset_count'])
            vuln_counts.append(solution['vuln_count'])
            exploit_code_maturity.append(solution['exploit_code_maturity'])
            vpr_scores.append(solution['vpr_max'])
            cvss_max_scores.append(solution['cvss_max'])

        header_color = 'white'
        row_even_color = 'lightgrey'
        row_odd_color = 'lightblue'
        fig = go.Figure(
            data=[go.Table(
                header=dict(
                    values=[
                        'Title',
                        'Solution',
                        'Asset Count',
                        'Vulnerability Count',
                        'Exploit Code Maturity',
                        'VPR',
                        'CVSS',
                    ],
                    line_color='darkslategray',
                    fill_color=header_color,
                    align=['center']
                ),
                cells=dict(
                    values=[descriptions, titles, asset_counts, vuln_counts, exploit_code_maturity, vpr_scores, cvss_max_scores],
                    line_color='darkslategray',
                    fill_color='ivory',
                    align=['center']
                )
            )]
        )

        # Making the figure big to see more data
        fig.update_layout(width=2560, height=1440)
        fig.show()


def pick_tag_from_list(client):
    tags = client.get_available_tags()
    tag_list = list()
    counter = 1
    print("Enter the number of the tag you want to select: ")
    for tag in tags:
        tag_list.append({
            'category_name': tag['category_name'], 
            'value_name': tag['value_name'], 
            'value_id': tag['value_id'],
        })
        print(counter, tag['category_name'], tag['value_name'])
        counter = counter + 1
    selected_tag = int(input(' >> '))
    if selected_tag == '' or selected_tag == ' ':
        selected_tag = 'Your Organization'
    elif selected_tag > len(tag_list):
        sys.exit("Number is out of range. Exiting.")
    elif selected_tag < len(tag_list):
        sys.exit("Number is out of range. Exiting.")
    else:
        selected_tag = selected_tag - 1
        print(selected_tag, tag_list[selected_tag]['value_id'])
    return tag_list[selected_tag]['value_id']

if __name__ == '__main__':
    lumin = Lumin()
    lumin.get_ces_by_asset_group(pick_tag_from_list(lumin))
