# Export Lumin Data

## Environment Setup

1. Retrieve API and Access keys from https://cloud.tenable.com
2. Create a `settings.ini` file and follow this format:

    [settings]
    ACCESS_KEY=YOUR-ACCESS-KEY
    SECRET_KEY=YOUR-SECRET-KEY

3. Create a virtual environment using `virtualenv` and use Python 3.6+
    - `virtualenv .venv -p $(which python3)` (or you can type in the full path of your python 3.6+ executable)
4. Activate your virtual environment
    - `. .venv/bin/activate` (on Mac OS X and Linux)
    - `.venv/Scripts/activate` (on Windows)
4. Run `pip install -r requirements.txt` to get the dependencies
    - Main dependencies include `python-decouple, requests, plotly`
5. To run the script, type `python app.py`

Any questions, please email Omar at oquimbaya@tenable.com.

Currently in a proof of concept stage.
